<?php

namespace GT\Common\Constants;

class WebConstants
{
    const FIND_ZIP_CODE_LIST_ACTION_GET_DISTRICT = "get_district";
    const FIND_ZIP_CODE_LIST_ACTION_GET_SUB_DISTRICT = "get_sub_district";
    const FIND_ZIP_CODE_LIST_ACTION_GET_ZIP_CODE = "get_zip_code";

    const ENV_KEY_OMISE_PUBLIC_KEY = "OMISE_PUBLIC_KEY";

    const ORDER_STATUS_CREATED = "คำสั่งใหม่";
    const ORDER_STATUS_WAIT_CUSTOMER = "รอการติดต่อจากลูกค้า";
    const ORDER_STATUS_WAIT_SELLER = "รอการติดต่อจากผู้ขาย";
    const ORDER_STATUS_WAIT_US = "รอการดำเนินงานของเรา";
    const ORDER_STATUS_CLOSED = "จบขั้นตอน";
}
