<?php

namespace GT\Common\Constants;

class DBConstants
{
    const USER_KEY_STATUS = 'status';
    const USER_STATUS_PENDING_EMAIL_VERIFICATION = 'pending_email_verification';
    const USER_STATUS_ACTIVE = 'active';
    const SELLER_STATUS_PENDING_APPROVAL = 'pending_approval';
    const SELLER_STATUS_ACTIVE = 'active';
    const ORDER_STATUS_CREATED = 'created';
    const ORDER_STATUS_AUTHORIZED = 'authorized';
    const ORDER_STATUS_UNAUTHORIZED = 'unauthorized';
    const ORDER_STATUS_OMISE_EXCEPTION = 'omise_exception';
    const ORDER_STATUS_PROCESSED_NO_PROPOSAL = 'processed_no_proposal';
    const ORDER_STATUS_MATCHED = 'matched';
    const ORDER_STATUS_MATCHED_BUT_CHARGE_FAILED = 'matched_but_charge_failed';
    const ORDER_STATUS_MATCHED_UPDATE_DB_FAILED = 'matched_update_db_failed';
    const ORDER_STATUS_OTHER_EXCEPTION = 'other_exception';

    const ORDER_STATUS_WAIT_CUSTOMER = "wait_customer";
    const ORDER_STATUS_WAIT_SELLER = "wait_seller";
    const ORDER_STATUS_WAIT_US = "wait_us";
    const ORDER_STATUS_CLOSED = "closed";

    const ORDER_CHARGE_STATUS_PENDING = 'pending'; //this is same as actual omise status.
    const ORDER_CHARGE_STATUS_CHARGED = 'charged';
    const ORDER_TYPE_SET_MY_PRICE = 'set_my_price';
    const ORDER_TYPE_VOLUME_SALE = 'volume_sale';
    const ORDER_TYPE_UPLOAD_INVOICE = 'upload_invoice';
    const PROPOSAL_STATUS_ACTIVE = 'active';
    const PROPOSAL_STATUS_PROCESSED = 'processed';
    const WIN_TABLE_STATUS_NEW_WINNER = 'new_winner';
    const USER_ADDRESS_ADDRESS_TYPE_SHIPPING = 'shipping';
    const USER_ADDRESS_ADDRESS_TYPE_BILLING = 'billing';
    
    const UPLOADED_FILE_TYPE_IMAGE = "image";
    const UPLOADED_FILE_TYPE_OTHER = "other";
    
}
