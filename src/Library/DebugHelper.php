<?php

namespace GT\Common\Library;

class DebugHelper {
    static $SKIP_AUTH = false;
    static $SKIP_VALIDATION = false;
    static $PRINT_VAR = false;


    public static function isSkipAuth() {
        return self::$SKIP_AUTH;
    }

    public static function isSkipValidation() {
        return self::$SKIP_VALIDATION;
    }

    public static function isPrintVar() {
        return self::$PRINT_VAR;
    }
}
?>