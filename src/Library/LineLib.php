<?php

namespace GT\Common\Library;

use Illuminate\Support\Facades\Log;
use LINE\LINEBot;
use LINE\LINEBot\Constant\MessageType;
use LINE\LINEBot\MessageBuilder\TextMessageBuilder;
use LINE\Tests\LINEBot\Util\DummyHttpClient;

class LineLib {

    function __construct(String $secret, String $token) {
        $this->secret = $secret;
        $this->token = $token;
    }
    
    /**
     * returns true or false
     */
    public function send_message(String $line_id, String $text) {
        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($this->token);
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => $this->secret]);
  
        $res = $bot->pushMessage($line_id, new TextMessageBuilder($text));
        
        //if bad Line Hex ID, Line will return 400 and isSucceeded() == false;
        $httpStatus = $res->getHTTPStatus();
        Log::info($httpStatus);
        
        $succeeded = $res->isSucceeded();
        //Log::info($succeeded);
        //Log::info($res->getJSONDecodedBody());
        if ($succeeded) {
            $jsonBody = $res->getJSONDecodedBody();
            Log::info($jsonBody);
        } else {
            Log::info('LineLib send_message error line_id=' . $line_id . ' text=' . $text);
            Log::info('LineLib send_message error ' . json_encode($res->getJSONDecodedBody()));
        }
        return $succeeded;
        
        //presume http client is closed automatically?
    }
}
?>
