<?php

namespace GT\Common\Library;

use GT\Common\Model\Address;
use GT\Common\Model\Product;
use GT\Common\Model\ZipCodeList;
use GT\Common\Constants\DBConstants;
use GT\Common\Constants\WebConstants;

class CommonLib {

    public static function pretty_print_currency($n) {
        return CommonLib::get_str_with_commas_from_number($n);
    }

    /**
     * Return number with comma.
     * @param $n int or string
     * @return string
     */
    public static function get_str_with_commas_from_number($n) {
        if (is_null($n)) {
            return '';
        } else {
            $s = (string) $n;
            $r = '';
            $count = 0;
            for ($index=strlen($s) - 1; $index >= 0; $index--) {
                $c = $s[$index];
                if ($count == 2 && $index != 0) {
                    $r = ',' . $c . $r;
                    $count = 0;
                } else {
                    $r = $c . $r;
                    $count += 1;
                }
            }
            return $r;
        }
    }

    /**
     * Return number from string of number with commas.
     * @param $s a string
     * @return int
     */
    public static function get_number_from_commas_str($s) {
        return intval(str_replace(",", "", $s));
    }

    public static function genRandomString($length, $characters = '0123456789') {
        //$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        //$characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function gen_password_hash($password) {
        //return password_hash($password, PASSWORD_DEFAULT);
        return \Hash::make($password);
    }

    public static function convert_to_address_json($a_1, $a_2, $sub_district, $district, $province,
                                                   $postal_code) {
        $r = [];
        $r['addr_1'] = $a_1;
        $r['addr_2'] = $a_2;
        $r['sub_district'] = $sub_district;
        $r['district'] = $district;
        $r['province'] = $province;
        $r['postal_code'] = $postal_code;
        return $r;
    }

    public static function find_or_insert_address($a_1, $a_2, $sub_district, $district, $province,
                                                  $postal_code) {
        //$a_2 can be null
        $found = false;
        $existings = Address::where('addr_1', $a_1)->where('sub_district', $sub_district)->where('district', $district)
            ->where('province', $province)->where('postal_code', $postal_code)->get();
        foreach ($existings as $existing) {
            if ($existing->addr_2 === $a_2) {
                $found = true;
                return $existing;
            }
        }
        //$found must be false;
        $new_row = Address::create(CommonLib::convert_to_address_json($a_1, $a_2, $sub_district, $district, $province,
            $postal_code));
        return $new_row;
    }

    //bring tier 1 provinces to front of array
    public static function sort_cmp_province_arr($a, $b)
    {
        if ($a === $b) {
            return 0;
        }
        //print($a . ' ' . $b . "<br>");
        if ($a === 'กรุงเทพมหานคร' || $b === 'กรุงเทพมหานคร') {
            if ($a === 'กรุงเทพมหานคร')
                return -1;
            else
                return 1;
        }
        elseif ($a === 'นครปฐม' || $b === 'นครปฐม') {
            if ($a === 'นครปฐม')
                return -1;
            else
                return 1;
        }
        elseif ($a === 'นนทบุรี' || $b === 'นนทบุรี') {
            if ($a === 'นนทบุรี')
                return -1;
            else
                return 1;
        }
        elseif ($a === 'ปทุมธานี' || $b === 'ปทุมธานี') {
            if ($a === 'ปทุมธานี')
                return -1;
            else
                return 1;
        }
        elseif ($a === 'สมุทรปราการ' || $b === 'สมุทรปราการ') {
            if ($a === 'สมุทรปราการ')
                return -1;
            else
                return 1;
        }
        elseif ($a === 'สมุทรสาคร' || $b === 'สมุทรสาคร') {
            if ($a === 'สมุทรสาคร')
                return -1;
            else
                return 1;
        }
        elseif ($a === 'ชลบุรี' || $b === 'ชลบุรี') {
            if ($a === 'ชลบุรี')
                return -1;
            else
                return 1;
        }
        else {
            //print($a . $b . "<br>");
            if ($a < $b) {
                return -1;
            } else {
                if ($a === $b) {
                    return 0;
                } else {
                    return 1;
                }
            }
        }

    }

    public static function get_province_arr($for_shipping=false) {
        $allowed_shipping_provinces = [
            'กรุงเทพมหานคร', 'นครปฐม', 'ชลบุรี', 'ฉะเชิงเทรา', 'ปทุมธานี', 'นนทบุรี',
            'สมุทรปราการ', 'สมุทรสาคร',
            'สมุทรสงคราม'
        ];
        $province_arr = [];
        $rows = ZipCodeList::select('province_name')->groupBy('province_name')->get();

        foreach ($rows as $row) {
            if ($for_shipping) {
                $allowed = false;
                foreach ($allowed_shipping_provinces as $allowed_province) {
                    if ($allowed_province === $row->province_name) {
                        $allowed = true;
                        break;
                    }
                }
                if ($allowed) {
                    array_push($province_arr, $row->province_name);
                }
            } else {
                array_push($province_arr, $row->province_name);
            }
        }
        //$commonLib = new CommonLib();
        //sort the output
        usort($province_arr, array('GT\Common\Library\CommonLib', 'sort_cmp_province_arr'));
        return $province_arr;
    }

    public static function find_zip_list($request)
    {
        //request is province, district, sub_district
        //zip_code_list is province, amphur, district
        $r = [];
        $r['data'] = [];
        $cmd = $request['cmd'];
        if ($cmd === WebConstants::FIND_ZIP_CODE_LIST_ACTION_GET_DISTRICT) {
            $rows = ZipCodeList::where('province_name', $request['province'])
                ->select('amphur_name')->groupBy('amphur_name')->get();

            foreach ($rows as $row) {
                if (!empty($row->amphur_name))
                    array_push($r['data'], $row->amphur_name);
            }
        } elseif ($cmd === WebConstants::FIND_ZIP_CODE_LIST_ACTION_GET_SUB_DISTRICT) {
            $rows = ZipCodeList::where('province_name', $request['province'])
                ->where('amphur_name', $request['district'])
                ->select('district_name')->groupBy('district_name')->get();

            foreach ($rows as $row) {
                if (!empty($row->district_name))
                    array_push($r['data'], $row->district_name);
            }
        }
        elseif ($cmd === WebConstants::FIND_ZIP_CODE_LIST_ACTION_GET_ZIP_CODE) {
            $rows = ZipCodeList::where('province_name', $request['province'])
                ->where('amphur_name', $request['district'])
                ->where('district_name', $request['sub_district'])
                ->select('zip_code')->groupBy('zip_code')->get();

            foreach ($rows as $row) {
                if (!empty($row->zip_code))
                    array_push($r['data'], $row->zip_code);
            }
        }
        //TODO status field

        return $r;
    }

    /**
     * Get list of unique brands.
     * @return dict with 2 keys: accepted_brands_th and accepted_brands_en. Value of each key is an array.
     */
    public static function get_all_brands() {
        $r = [];
        $brand_rows = Product::where('parent_id', 1)->get();
        $brands_th = [];
        $brands_en = [];

        //use English
        foreach ($brand_rows as $brand_row) {
            $found = false;
            foreach ($brands_en as $current_brand_en) {
                if ($current_brand_en === $brand_row->name_en) {
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $brands_th[] = $brand_row->name_th;
                $brands_en[] = $brand_row->name_en;
            }
        }
        $r['accepted_brands_th'] = $brands_th;
        $r['accepted_brands_en'] = $brands_en;
        return $r;
    }

    /**
     * Get brand IDs from checkboxes.
     *
     * @param $data an array or request input array.
     * @return array of brand ids;
     */
    public static function get_brands_from_checkboxes($data) {
        $accept_brand_ids = [];
        foreach ($data as $k => $v) {
            if (strpos($k, 'accept_brand') === 0) {
                $brand_en = substr($k, strrpos($k, '_')+1, strlen($k) - strrpos($k, '_'));
                //value must be true. no need to check.
                $product_row = Product::where('parent_id', 1)->where('name_en', $brand_en)->first();
                //print_r($product_row);
                $accept_brand_ids[] = $product_row->id;
            }
        }
        if (DebugHelper::isPrintVar()) {
            //print_r($accept_brand_ids);
        }
        return $accept_brand_ids;
    }

    /** $data is an array or request input array.
     */
    public static function get_brand_names_from_id_array($arr) {
        $r = [];
        foreach ($arr as $k => $v) {
            $product_row = Product::where('id', $v)->first();
            $r[] = $product_row->name_th;
        }
        if (DebugHelper::isPrintVar()) {
            //print_r($r);
        }
        return $r;
    }

    public static function get_omise_public_key() {
        return getenv(WebConstants::ENV_KEY_OMISE_PUBLIC_KEY);
    }

    public static function get_db_constants_from_web_constants($web_const) {
        switch ($web_const) {
            case (WebConstants::ORDER_STATUS_CREATED): return DBConstants::ORDER_STATUS_CREATED;
            case (WebConstants::ORDER_STATUS_WAIT_CUSTOMER): return DBConstants::ORDER_STATUS_WAIT_CUSTOMER;
            case (WebConstants::ORDER_STATUS_WAIT_SELLER): return DBConstants::ORDER_STATUS_WAIT_SELLER;
            case (WebConstants::ORDER_STATUS_WAIT_US): return DBConstants::ORDER_STATUS_WAIT_US;
            case (WebConstants::ORDER_STATUS_CLOSED): return DBConstants::ORDER_STATUS_CLOSED;
        }
        return null;
    }

    public static function get_web_constants_from_db_constants($db_const) {
        switch ($db_const) {
            case (DBConstants::ORDER_STATUS_CREATED): return WebConstants::ORDER_STATUS_CREATED;
            case (DBConstants::ORDER_STATUS_WAIT_CUSTOMER): return WebConstants::ORDER_STATUS_WAIT_CUSTOMER;
            case (DBConstants::ORDER_STATUS_WAIT_SELLER): return WebConstants::ORDER_STATUS_WAIT_SELLER;
            case (DBConstants::ORDER_STATUS_WAIT_US): return WebConstants::ORDER_STATUS_WAIT_US;
            case (DBConstants::ORDER_STATUS_CLOSED): return WebConstants::ORDER_STATUS_CLOSED;
        }
        return null;
        return null;
    }
}
?>
