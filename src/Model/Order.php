<?php

namespace GT\Common\Model;

use GT\Common\Constants\DBConstants;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
        'my_created_at',
        'end_order_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function productDetail()
    {
        return $this->belongsTo('GT\Common\Model\ProductDetail');
    }

    public function is_volume_sale() {
        return strcmp($this->order_type, DBConstants::ORDER_TYPE_VOLUME_SALE) == 0;
    }

    public function is_set_my_price() {
        return strcmp($this->order_type, DBConstants::ORDER_TYPE_SET_MY_PRICE) == 0;
    }
}
