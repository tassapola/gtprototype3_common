<?php

namespace GT\Common\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';

    public function productDetail()
    {
        return $this->hasOne('GT\Common\Model\ProductDetail');
    }
}
