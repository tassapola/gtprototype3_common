<?php

namespace GT\Common\Model;

use Illuminate\Database\Eloquent\Model;

class LineSeller extends Model
{
    protected $table = 'line_seller';
}
