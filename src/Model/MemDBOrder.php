<?php

namespace GT\Common\Model;

use GT\Common\Constants\DBConstants;
use GT\Common\Model\Order;
use GT\Common\Model\Product;
use GT\Common\Model\MemAddress;

class MemDBOrder {

    public $order_type;

    //brands data depend on order_type
    public $brands;
    const BRAND_KEY_ID = 'id';
    const BRAND_KEY_NAME = 'name';

    const DB_MAX_NUM_ITEMS = 5;
    const DB_MAX_NUM_BRANDS = 10;
    const ITEM_KEY_PRODUCT_DETAIL = 'product_detail'; //product id, only in set-my-price and e-commerce
    const ITEM_KEY_CATEGORY_1 = 'category_1_name_th'; //Air-con
    const ITEM_KEY_CATEGORY_2 = 'category_2_name_th'; //Brand
    const ITEM_KEY_CATEGORY_3 = 'category_3_name_th'; //Air type
    const ITEM_KEY_CATEGORY_4 = 'category_4_name_th'; //Air sub type
    const ITEM_KEY_CATEGORY_5 = 'category_5_name_th'; //BTU
    const ITEM_KEY_CATEGORY_6 = 'category_6_name_th'; //model
    const ITEM_KEY_UNITS = 'units';
    const ITEM_KEY_PRICE_PER_UNIT = 'price_per_unit';

    /**
     * @var array items[0..num_item-1]
     *
     * data depends on ser
     */
    public $items;

    public $total_units;
    public $shipping_cost;
    public $total_price_incl_shipping;
    public $wait_hours;
    public $ref_id;
    public $my_created_at;
    public $end_order_at;
    public $status;
    public $charge_charge_id;
    public $charge_description;
    public $charge_amount;

    public $buyer_shipping_address;
    public $buyer_billing_address;

    public $user_id;

    public function is_volume_sale() {
        return strcmp($this->order_type, DBConstants::ORDER_TYPE_VOLUME_SALE) == 0;
    }

    public function is_set_my_price() {
        return strcmp($this->order_type, DBConstants::ORDER_TYPE_SET_MY_PRICE) == 0;
    }

    public function __construct(Order $order) {
        $this->order_type = $order->order_type;
        //populate brands for volume sale
        if ($this->is_volume_sale()) {
            $brands = [];
            for ($brand_no=1; $brand_no <= MemDBOrder::DB_MAX_NUM_BRANDS; $brand_no++) {
                $brand = [];
                if (isset($order['volume_sale_brand_id_' . $brand_no])) {
                    $brand[MemDBOrder::BRAND_KEY_ID] = $order['volume_sale_brand_id_' . $brand_no];
                    $product_row = Product::where('id', $brand[MemDBOrder::BRAND_KEY_ID])->first();

                    //var_dump($product_row);
                    if (empty($product_row)) {
                        $name_th = '';
                    } else {
                        $name_th = $product_row->name_th;
                    }
                    $brand[MemDBOrder::BRAND_KEY_NAME] = $name_th;
                    $brands[] = $brand;
                }
            }

            $this->brands = $brands;
        } else {
            $brand = [];
            $brand[MemDBOrder::BRAND_KEY_NAME] = $order['item_1_category_2_name_th'];
            $brand[MemDBOrder::BRAND_KEY_ID] = Product::where('name_th', $brand[MemDBOrder::BRAND_KEY_NAME])->first()->id;
            $this->brands = [ $brand ];
        }

        //populate items
        $items=[];
        for ($item_no=1; $item_no <= MemDBOrder::DB_MAX_NUM_ITEMS; $item_no++) {
            $db_field_prefix = 'item_' . $item_no . '_';
            if ($this->is_volume_sale()) {
                $keys = [MemDBOrder::ITEM_KEY_CATEGORY_3, MemDBOrder::ITEM_KEY_CATEGORY_4, MemDBOrder::ITEM_KEY_CATEGORY_5,
                    MemDBOrder::ITEM_KEY_UNITS, MemDBOrder::ITEM_KEY_PRICE_PER_UNIT];
            } else {
                $keys = [ MemDBOrder::ITEM_KEY_CATEGORY_1, MemDBOrder::ITEM_KEY_CATEGORY_2,
                          MemDBOrder::ITEM_KEY_CATEGORY_3, MemDBOrder::ITEM_KEY_CATEGORY_4, MemDBOrder::ITEM_KEY_CATEGORY_5,
                          MemDBOrder::ITEM_KEY_CATEGORY_6,
                          MemDBOrder::ITEM_KEY_UNITS, MemDBOrder::ITEM_KEY_PRICE_PER_UNIT];
            }
            if (isset($order[$db_field_prefix . MemDBOrder::ITEM_KEY_UNITS])) {
                $item = [];
                foreach ($keys as $key) {
                    $item[$key] = $order[$db_field_prefix . $key];
                }
                $items[] = $item;
            }
        }

        
        $this->items = $items;

        $this->total_units = $order->total_units;
        $this->shipping_cost = $order->shipping_cost;
        $this->total_price_incl_shipping = $order->total_price_incl_shipping;
        $this->wait_hours = $order->wait_hours;
        $this->ref_id = $order->ref_id;
        $this->my_created_at = $order->my_created_at;
        $this->end_order_at = $order->end_order_at;
        $this->status = $order->status;
        $this->charge_charge_id = $order->charge_charge_id;
        $this->charge_description = $order->charge_description;
        $this->charge_amount = $order->charge_amount;

        //buyer addresses
        $this->buyer_billing_address = new MemAddress($order->buyer_address_bill_id);
        $this->buyer_shipping_address = new MemAddress($order->buyer_address_ship_id);

        $this->user_id = $order->user_id;
    }

    public function get_buyer_billing_address() {
        return $this->buyer_billing_address;
    }

    public function get_buyer_shipping_address() {
        return $this->buyer_shipping_address;
    }

    public function get_user_id() {
        return $this->user_id;
    }

    public function get_ref_id() {
        return $this->ref_id;
    }

    public function get_order_type()
    {
        return $this->order_type;
    }

    public function get_brand_ids()
    {
        $r = [];
        foreach ($this->brands as $brand) {
            $r[] = $brand[MemDBOrder::BRAND_KEY_ID];
        }
        return $r;
    }

    public function get_brand_names()
    {
        $r = [];
        foreach ($this->brands as $brand) {
            $r[] = $brand[MemDBOrder::BRAND_KEY_NAME];
        }
        return $r;
    }

    public function get_brand_names_as_html() {
        $r = '<ol>';
        $brand_names = $this->get_brand_names();
        for ($i=0; $i < count($brand_names); $i++) {
            $item_no = $i + 1;
            $r = $r . "<li>" . $brand_names[$i] . "</li>";
        }
        $r .= '</ol>';
        return $r;
    }

    /**
     * @return array
     */
    public function get_items()
    {
        return $this->items;
    }

    public function get_items_as_html() {
        $r = '';
        $items = $this->items;
        for ($i=0; $i < count($items); $i++) {
            $item_no = $i + 1;
            $item = $items[$i];
            $r .= $item_no . '. ';
            if ($this->is_set_my_price()) {
                $r .= $item[MemDBOrder::ITEM_KEY_CATEGORY_2] . ' / ';
            }
            $r .= $item[MemDBOrder::ITEM_KEY_CATEGORY_3] . ' / ';
            $r .= $item[MemDBOrder::ITEM_KEY_CATEGORY_4] . ' / ';
            $r .= $item[MemDBOrder::ITEM_KEY_CATEGORY_5] . ' / ';
            if ($this->is_set_my_price()) {
                $r .= $item[MemDBOrder::ITEM_KEY_CATEGORY_6] . ' / ';
            }
            $r .= $item[MemDBOrder::ITEM_KEY_UNITS] . ' ตัว / ';
            $r .= CommonLib::get_str_with_commas_from_number($item[MemDBOrder::ITEM_KEY_PRICE_PER_UNIT]) . ' บาทต่อตัว';
            if ($i < count($items) - 1) {
                $r .= '<br>';
            }
        }
        return $r;
    }

    public function get_items_as_list_html() {
        $r = '<ol>';
        $items = $this->items;
        for ($i=0; $i < count($items); $i++) {
            $item_no = $i + 1;
            $item = $items[$i];
            $r .= '<li>';
            if ($this->is_set_my_price()) {
                $r .= $item[MemDBOrder::ITEM_KEY_CATEGORY_2] . ' / ';
            }
            $r .= $item[MemDBOrder::ITEM_KEY_CATEGORY_3] . ' / ';
            $r .= $item[MemDBOrder::ITEM_KEY_CATEGORY_4] . ' / ';
            $r .= $item[MemDBOrder::ITEM_KEY_CATEGORY_5] . ' / ';
            if ($this->is_set_my_price()) {
                $r .= $item[MemDBOrder::ITEM_KEY_CATEGORY_6] . ' / ';
            }
            $r .= $item[MemDBOrder::ITEM_KEY_UNITS] . ' ตัว / ';
            $r .= CommonLib::get_str_with_commas_from_number($item[MemDBOrder::ITEM_KEY_PRICE_PER_UNIT]) . ' บาทต่อตัว';
            if ($i < count($items) - 1) {
                $r .= '</li>';
            }
        }
        $r .= '</ol>';
        return $r;
    }

    public function get_total_units()
    {
        return $this->total_units;
    }

    public function get_shipping_cost()
    {
        return $this->shipping_cost;
    }

    public function get_total_price_incl_shipping()
    {
        return $this->total_price_incl_shipping;
    }

    public function get_wait_hours()
    {
        return $this->wait_hours;
    }

    public function get_my_created_at()
    {
        return $this->my_created_at;
    }

    public function get_end_order_at()
    {
        return $this->end_order_at;
    }

    public function get_status()
    {
        return $this->status;
    }

    public function get_charge_charge_id()
    {
        return $this->charge_charge_id;
    }

    public function get_charge_description()
    {
        return $this->charge_description;
    }

    public function get_charge_amount() {
        return $this->charge_amount;
    }



}

?>