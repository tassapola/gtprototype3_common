<?php

namespace GT\Common\Model;

use Illuminate\Database\Eloquent\Model;

class OrderComment extends Model
{
    protected $table = 'order_comment';
}
