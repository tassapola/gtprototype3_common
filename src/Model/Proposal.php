<?php

namespace GT\Common\Model;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
        'my_created_at'
    ];
}

