<?php

namespace GT\Common\Model;

use GT\Common\Model\Order;
use GT\Common\Model\Buyer;

class MemUserInfo {

    public $first_name_th;
    public $last_name_th;
    public $email;
    public $line_id;
    public $status;
    public $company_name;
    public $tax_id;
    public $primary_address_bill_id;
    public $primary_address_ship_id;
    public $tel_number;
    public $mobile_number;
    
    public function __construct(int $user_id) {
        $buyer = Buyer::where('id', $user_id)->first();
        //var_dump($buyer);
        if (empty($buyer)) {

        } else {
            $this->first_name_th = $buyer->first_name_th;
            $this->last_name_th = $buyer->last_name_th;
            $this->email = $buyer->email;
            $this->line_id = $buyer->line_id;
            $this->status = $buyer->status;
            $this->company_name = $buyer->company_name;
            $this->tax_id = $buyer->tax_id;
            $this->primary_address_bill_id = $buyer->primary_address_bill_id;
            $this->primary_address_ship_id = $buyer->primary_address_ship_id;
            $this->tel_number = $buyer->tel_number;
            $this->mobile_number = $buyer->mobile_number;
        }


    }

    public function get_email() {
        return $this->email;
    }

    public function get_full_name_th() {
        return $this->first_name_th . ' ' . $this->last_name_th;
    }

    public function get_first_name_th()
    {
        return $this->first_name_th;
    }

    public function get_last_name_th()
    {
        return $this->last_name_th;
    }

    public function get_line_id()
    {
        return $this->line_id;
    }

    public function get_status()
    {
        return $this->status;
    }

    public function get_company_name()
    {
        return $this->company_name;
    }

    public function get_tax_id()
    {
        return $this->tax_id;
    }

    public function get_tel_number()
    {
        return $this->tel_number;
    }


    public function get_mobile_number()
    {
        return $this->mobile_number;
    }

}

?>