<?php

namespace GT\Common\Model;

use Illuminate\Database\Eloquent\Model;

class SellerDocs extends Model
{
    protected $table = 'seller_docs';
}
