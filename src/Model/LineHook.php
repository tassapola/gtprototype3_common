<?php

namespace GT\Common\Model;

use Illuminate\Database\Eloquent\Model;

class LineHook extends Model
{
    protected $table = 'line_hook';
}
