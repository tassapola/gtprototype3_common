<?php

namespace GT\Common\Model;

use Illuminate\Database\Eloquent\Model;

class FBLogin extends Model
{
    protected $table = 'fb_login';
}
