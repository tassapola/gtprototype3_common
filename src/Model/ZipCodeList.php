<?php

namespace GT\Common\Model;

use Illuminate\Database\Eloquent\Model;

class ZipCodeList extends Model
{
    protected $table = 'zip_code_list';

}
