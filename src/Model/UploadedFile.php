<?php

namespace GT\Common\Model;

use GT\Common\Constants\DBConstants;
use Illuminate\Database\Eloquent\Model;

class UploadedFile extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
        'my_created_at',
    ];

    public function order()
    {
        return $this->belongsTo('GT\Common\Model\Order');
    }

    public function is_image() {
        return strcmp($this->order_type, DBConstants::UPLOADED_FILE_TYPE_IMAGE) == 0;
    }

    public function is_other() {
        return strcmp($this->order_type, DBConstants::UPLOADED_FILE_TYPE_OTHER) == 0;
    }
}
