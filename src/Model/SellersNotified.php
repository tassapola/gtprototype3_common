<?php

namespace GT\Common\Model;

use Illuminate\Database\Eloquent\Model;

class SellersNotified extends Model
{
    protected $table = 'sellers_notified';
}
