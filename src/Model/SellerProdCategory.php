<?php

namespace GT\Common\Model;

use Illuminate\Database\Eloquent\Model;

class SellerProdCategory extends Model
{
    protected $table = 'seller_prod_category';
}
