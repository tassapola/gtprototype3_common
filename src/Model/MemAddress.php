<?php

namespace GT\Common\Model;

use GT\Common\Model\MemDBOrder;
use GT\Common\Model\Address;

class MemAddress {

    const KEY_ADDRESS_1 = "1";
    const KEY_ADDRESS_2 = "2";
    const KEY_ADDRESS_SUB_DISTRICT = "sub_district";
    const KEY_ADDRESS_DISTRICT = "district";
    const KEY_ADDRESS_PROVINCE = "province";
    const KEY_ADDRESS_POSTAL_CODE = "postal_code";

    public $addr;

    public function __construct(int $user_address_id) {
        $a = Address::where('id', $user_address_id)->first();
        if (empty($a)) {
            $this->addr = null;
        } else {
            $addr = [];
            $addr[MemAddress::KEY_ADDRESS_1] = $a->addr_1;
            $addr[MemAddress::KEY_ADDRESS_2] = $a->addr_2;
            $addr[MemAddress::KEY_ADDRESS_SUB_DISTRICT] = $a->sub_district;
            $addr[MemAddress::KEY_ADDRESS_DISTRICT] = $a->district;
            $addr[MemAddress::KEY_ADDRESS_PROVINCE] = $a->province;
            $addr[MemAddress::KEY_ADDRESS_POSTAL_CODE] = $a->postal_code;
            $this->addr = $addr;
        }
    }

    public function get_as_str() {
        $r = '';
        $r = $r . $this->addr[MemAddress::KEY_ADDRESS_1];
        if (!empty($this->addr[MemAddress::KEY_ADDRESS_2])) {
            $r = $r . ' ' . $this->addr[MemAddress::KEY_ADDRESS_2];
        }
        $r = $r . ' ' . $this->addr[MemAddress::KEY_ADDRESS_SUB_DISTRICT];
        $r = $r . ' ' . $this->addr[MemAddress::KEY_ADDRESS_DISTRICT];
        $r = $r . ' ' . $this->addr[MemAddress::KEY_ADDRESS_PROVINCE];
        $r = $r . ' ' . $this->addr[MemAddress::KEY_ADDRESS_POSTAL_CODE];
        return $r;
    }
}
?>