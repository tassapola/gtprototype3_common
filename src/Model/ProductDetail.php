<?php

namespace GT\Common\Model;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    public function product()
    {
        return $this->belongTo('GT\Common\Model\Product');
    }
}
